<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php print $pageTitle; ?></title>
        <link rel="stylesheet" type="text/css" href="<?php print $siteRoot; ?>/css/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?php print $siteRoot; ?>/css/fonts.css" />
        <link rel="stylesheet" type="text/css" href="<?php print $siteRoot; ?>/css/screen.css" />
        <script src="<?php print $siteRoot; ?>/js/jquery-1.10.2.min.js"></script>
        <script src="<?php print $siteRoot; ?>/js/jquery-ui-1.10.4.min.js"></script>
        <script src="<?php print $siteRoot; ?>/js/main.js"></script>
    </head>
    <body>