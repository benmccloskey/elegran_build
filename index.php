<?php
$siteRoot = '/elegran';
$queryString = strtolower($_GET['q']);
list($page, $arg1, $arg2, $arg3) = explode('/', $queryString);
$pageTitle = ucwords('Elegran Site Build - '. $page.' Page');

// include header
require_once('inc/header.php');

// include appropriate file, if it exists
if(file_exists('inc/'.$page.'.php')){
    require_once('inc/'.$page.'.php');
}else{
    print 'Aw man, you broke it :(';
}

//include footer
require_once('inc/footer.php');
?>